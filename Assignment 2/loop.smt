;;
;; By calling "z3 -smt2 loop . smt" one can check the assertion
;; in the following code using the loop invariant : m * x = n * y + z
;;
;; Igor Konnov , Josef Widder , 2013. v1 .0
;;
;; int n = input();
;; int x = input();
;; int m = n;
;; int y = x;
;; int z = 0;
;; assume (n >= 0);
;; while (n > 0) {
;; 	if (n % 2) {
;; 		z += y;
;; 	}
;; 	y *= 2;
;; 	n /= 2;
;; }
;; assert (z == m * x);
(declare-const n Int)
(declare-const x Int)
(declare-const m Int)
(declare-const y Int)
(declare-const z Int)
(declare-const n2 Int)
(declare-const y2 Int)
(declare-const z2 Int)
(define-fun loopcond () Bool (> n 0))
(define-fun loopbody () Bool
	(if loopcond
		(and (if (= 1 (mod n 2))
				(= z2 (+ z y))
			(= z2 z))
		(= y2 (* y 2))
		(= n2 (/ n 2)))
		(and (= z2 z)
		(= y2 y)
	(= n2 n))))
(define-fun invariant () Bool (and
		(>= n 0)
		(>= m 0)
		(= (* m x) (+ z (* n y)))))
(define-fun invariantpost () Bool (and
			(>= n2 0)
			(>= m 0)
			(= (* m x) (+ z2 (* n2 y2)))))
;;
;; check that the precondition implies invariant
;;
(push)
	(assert (not (=>
		(and (= m n) (= x y) (= z 0) (>= n 0))
		invariant
		)))
	(check-sat)
(pop)
;;
;; check invariant
;;
(push)
(assert (not (=>
			(and invariant loopbody)
			invariantpost)))
	(check-sat)
	;;(get-model)
(pop)
;;
;; check assertion after leaving loop
;;
(push)
	(assert (not (=>
		(and invariant (not loopcond))
		(= z (* m x)))))
	(check-sat)
	;; (get-model)
(pop)
;;
;; check that loop terminates (i.e., if n>0 then n decreases)
;;
(push)
	(assert (not (=>
		(and loopcond loopbody)
		(< n2 n))))
	(check-sat)
	;;(get-model)
(pop)