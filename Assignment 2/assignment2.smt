;;
;; Alexander Halbarth 1129193
;;
;; double S = input () ;
;; assert (0 < S && S < 3) ;
;; double a = S ;
;; double c = S - 1.0;
;; while ( c >= 0.05 || c <= -0.05) {
;;		a = a - a * c / 2.0;
;;		c = c * c * ( c - 3.0) / 4.0;
;; 		assert ( S * (1 + c ) = a * a && S > 0 && S < 3) ;
;; }
;; printf ( " sqrt of % f is approx . % f \ n " , S , a ) ;
;; assert ( a * a > S * 0.95);
;; assert ( a * a < S * 1.05);
(declare-const s Real)
(declare-const a Real)
(declare-const c Real)
(declare-const a2 Real)
(declare-const c2 Real)
(define-fun loopcond () Bool (or (>= c 0.05) (<= c -0.05)))
(define-fun loopbody () Bool
	(if loopcond
		(and 
			(= a2 (- a (/ (* a c) 2.0)))
			(= c2 (* c (* c (/ (- c 3.0) 4.0)))))
		(and (= a2 a)
		(= c2 c))))
(define-fun invariant () Bool 
	(and (= (* s (+ 1.0 c)) (* a a)) (> s 0.0) (< s 3.0)))
(define-fun invariantpost () Bool (and
	(and (= (* s (+ 1.0 c2)) (* a2 a2)) (> s 0.0) (< s 3.0))))
;;
;; check that the precondition implies invariant
;;
(push)
	(assert (not (=>
		(and (> s 0) (< s 3) (= a s) (= c (- s 1.0)))
		invariant
		)))
	(check-sat)
(pop)
;;
;; check invariant
;;
(push)
(assert (not (=>
			(and invariant loopbody)
			invariantpost)))
	(check-sat)
	;;(get-model)
(pop)
;;
;; check assertion after leaving loop
;;
(push)
	(assert (not (=>
		(and invariant (not loopcond))
		(and (> (* a a) (* s 0.95)) (< (* a a) (* s 1.05))))))
	(check-sat)
  ;;(get-model)
(pop)
;;
;; check that loop terminates (|c| decreases)
;;
(define-fun absolute ((x Real)) Real
  (ite (>= x 0) x (- x)))
(push)
	(assert (not (=>
		(and loopcond loopbody (> c -1) (< c 2))
		(< (absolute c2) (absolute c)))))
	(check-sat)
	;; (get-model)
(pop)