\documentclass[11pt,a4paper]{article}
\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{dsfont}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,angles,quotes}
\usepackage{listings}
\usepackage{ifthen}
\usepackage{paralist}

\parskip        0mm
\oddsidemargin  0in
\evensidemargin 0in
\textwidth      6in
\topmargin      0in
\marginparwidth 30pt
\textheight     9.2in
\headheight     .1in
\headsep        .1mm

\lstset{
  basicstyle=\footnotesize\ttfamily, % Standardschrift
  numberstyle=\tiny,          % Stil der Zeilennummern
  numbersep=5pt,              % Abstand der Nummern zum Text
  tabsize=2,                  % Groesse von Tabs
  extendedchars=true,         %
  breaklines=true,            % Zeilen werden Umgebrochen
  keywordstyle=\ttfamily,
  stringstyle=\ttfamily, % Farbe der String
  showspaces=false,           % Leerzeichen anzeigen ?
  showtabs=false,             % Tabs anzeigen ?
  xleftmargin=17pt,
  framexleftmargin=17pt,
  framexrightmargin=5pt,
  framexbottommargin=4pt,
  showstringspaces=false      % Leerzeichen in Strings anzeigen ?
}
\lstloadlanguages{
  C , C++, ml
}

\title{VU Programm- und Systemverifikation\\
\Large Assignment 1: Assertions, Testing, and Coverage}
\author{Name: \underline{Alexander Halbarth}\qquad
  Matr. number: \underline{1129193}}
\date{Due: April 5, 4pm (no extension!)}


\begin{document}

\maketitle

\section{Coverage Metrics}

Consider the following program fragment and test suite:\\
\noindent
\begin{tabular}{cc}
  \begin{minipage}{.45\textwidth}
    \begin{tabbing}
      \quad\=\quad\=\qquad\=\qquad\=\\\kill
      {\tt int maxsum (int max, int val) \{}\\
      \>{\tt int result = 0;}\\
      \>{\tt int i = 0;}\\
      \>{\tt if (val < 0)}\\
      \>\>{\tt val = -val;}\\
      \>{\tt while ((i < val) \&\& (result <= max)) \{}\\
      \>\>{\tt i = i + 1;}\\
      \>\>{\tt result = result + i;}\\
      \>{\tt \}}\\
      \>{\tt if (result <= max)}\\
      \>\>{\tt return result;}\\
      \>{\tt else}\\
      \>\>{\tt return max;}\\
      {\tt \}}\\
    \end{tabbing}
  \end{minipage} &
  \begin{minipage}{.45\textwidth}
    \begin{tabular}{|c|c||l|}
      \hline
      \multicolumn{2}{|c||}{Inputs} & \multicolumn{1}{|c|}{Outputs}\\
      \hline
      {\tt max} & {\tt val} & result\\
      \hline
      0 & 0& 0 \\\hline
      0 & -1 & 0 \\\hline
      10 & 1 & 1 \\\hline
    \end{tabular}
  \end{minipage}
\end{tabular}

\subsection{Control-Flow-Based Coverage Criteria (3 points)}

Indicate (\checkmark)
which of the following coverage criteria are satisfied by
the test-suite above (assume that the term ``decision'' refers to
all Boolean expressions in the program).
\begin{center}
  \begin{tabular}{|l|l|l|}
    \hline
    & \multicolumn{2}{|c|}{satisfied}\\
    \hline
    {\bf Criterion} & yes & no\\ \hline
    path coverage &&\checkmark\\\hline
    statement coverage &\checkmark&\\\hline
    branch coverage &&\checkmark\\\hline
    decision coverage &\checkmark&\\\hline
    condition/decision coverage &\checkmark&\\\hline
  \end{tabular}
\end{center}
For each coverage criterion that is \emph{not} satisfied, explain why
this is the case:\\
\textbf{path coverage}: The Loop makes path coverage impossible, since you cannot test every iteration!\\
\textbf{branch coverage}: First "else" (not going into if) then going into the loop and then else is not covered.
\vfill

\newpage


\subsection{Data-Flow-Based Coverage Criteria (5 points)}

Indicate (\checkmark)
which of the following coverage criteria are satisfied by
the test-suite above
(here, the parameters of the function do not constitute definitions,
and the {\tt return} statements are p-uses but not c-uses):

\begin{center}
  \begin{tabular}{|l|l|l|}
    \hline
    & \multicolumn{2}{|c|}{satisfied}\\
    \hline
    {\bf Criterion} & yes & no\\ \hline
    all-defs & \checkmark& \\    \hline
    all-c-uses & \checkmark& \\    \hline
    all-p-uses & &\checkmark \\    \hline
    all-c-uses/some-p-uses & \checkmark& \\     \hline
    all-p-uses/some-c-uses & &\checkmark \\     \hline
  \end{tabular}
\end{center}

For each coverage criterion that is not satisfied, explain why
this is the case:\\
\textbf{all-p-uses}: Before \emph{return result;} there is no definition \emph{val = -val} in any testcase.\\
\textbf{all-p-uses/some-c-uses}: Since \textbf{all-p-uses} is not fulfilled \textbf{all-p-uses/some-c-uses} cannot be fulfilled!

\vfill

\subsection{Modified Condition/Decision Coverage (1 point)}

\emph{If} the test-suite from above does not satisfy the
MC/DC coverage
criterion, augment it with the \emph{minimal} number of
test-cases such that this
criterion is satisfied. If full coverage cannot be achieved,
explain why.\\[1ex]

\noindent
\begin{center}
  \begin{minipage}{.4\textwidth}\centering
    {\bf MC/DC}\\[1ex]
    \begin{tabular}{|l|l||l|}
      \hline
      \multicolumn{2}{|c||}{Inputs} & \multicolumn{1}{|c|}{Outputs}\\
      \hline
      {\tt max} & {\tt val} & result\\
      \hline
      0 & 0& 0 \\
      0 & -1 & 0 \\
      10 & 1 & 1 \\
      -1 & 1 & -1 \\
			\hline
    \end{tabular}
  \end{minipage}
\end{center}

\newpage
\section{Equivalence Partitioning and Boundary Testing}

The function
\begin{center}
\begin{tabbing}
  \quad\=\kill
  {\tt enum \{ACUTE, RIGHT, OBTUSE, DEGENERATE\}}
   {\tt triangle\,(float $\alpha$, float $\beta$, float $\gamma$)}
\end{tabbing}
\end{center}
takes as parameters 3 angles $\alpha$, $\beta$, and $\gamma$
(in degrees) of a valid triangle and determines its type:
\begin{itemize}
\item acute: all angles are less than $90^\circ$,
\item right: has a right angle ($90^\circ$),
\item obtuse: has an angle that is larger than $90^\circ$,
\item degenerate: has angles that are $0^\circ$.
\end{itemize}
\begin{center}
  \begin{tikzpicture}
    \draw coordinate (a) --++(0:4cm) coordinate (b);

    \path[name path=ac] (a)--++(30:3cm);
    \path[name path=bc] (b)--++(180-37:3cm);

    \path [name intersections={of = ac and bc, by=c}];

%    \node[above] at (c) {c};

    \draw[use as bounding box] (a)--(b)--(c)--cycle%
    pic[draw, "$\alpha$", angle eccentricity=1.6] {angle=b--a--c}
    pic[draw, "$\gamma$", angle eccentricity=1.6] {angle=c--b--a}
    pic[draw, "$\beta$", angle eccentricity=1.6] {angle=a--c--b};
  \end{tikzpicture}
\end{center}

A valid triangle satisfies the following conditions:
\begin{itemize}
\item All angles are either 0 or positive values (in degrees), and
\item either exactly 2 or exactly 0 angles are $0^\circ$, and
\item the sum of all angles is the straight angle
  ($180^\circ$).
\end{itemize}

\subsection{Equivalence Partitioning (3.5 points)}

From the specification above, derive equivalence classes for the
method {\tt triangle}. Use the table below to partition them into
\emph{valid equivalence classes} (valid inputs) and
\emph{invalid equivalence classes} (invalid inputs).
Label each of the equivalence classes clearly with a number
(in the according column).
For each correct \underline{\emph{equivalence class}} you can score
$\frac{1}{2}$ a point (up to 3.5 points).\\
{\small (Do not provide test-cases here -- that's task 2.2)}\\[1ex]

\begin{tabular}{|p{.27\textwidth}|p{.25\textwidth}|p{.03\textwidth}|p{.25\textwidth}|p{.03\textwidth}|}
  \hline
  {\bf Condition} & {\bf Valid } & {\bf ID} & {\bf Invalid} & {\bf ID}\\
  \hline
	Angles 0 or positive & $\alpha \wedge \beta \wedge \gamma \geq 0 $&1& $\alpha \vee \beta \vee \gamma < 0$ & 2\\
	\hline
	2 or 0 angles are zero & $\alpha \, \textbf{XOR} \, \beta \, \textbf{XOR} \, \gamma > 0$ others $=0$&3&$\alpha \, \textbf{XOR} \, \beta \, \textbf{XOR} \, \gamma = 0$ others $>0$&4\\
	\cline{2-5}
	&											$\alpha \wedge \beta \wedge \gamma > 0$&5&&\\
	\hline
	$\prod angles = 180$ & $\alpha + \beta + \gamma = 180$& 6&$\alpha + \beta + \gamma \neq 180$&7\\
  \hline
\end{tabular}

\newpage

\subsection{Boundary Value Testing (3.5 points)}

Use \emph{Boundary Value Testing} to derive a test-suite for
the method {\tt triangle}. Specify the input angles
$\alpha$, $\beta$, and $\gamma$. Indicate clearly which equivalence
classes each test-case covers by referring to the numbers from
task 2.1. You can receive up to 3.5 points ($\frac{1}{2}$ a point per test-case),
where redundant test-cases and test-cases that do not represent boundary values
do not count.\\[1ex]

\begin{tabular}{|p{.35\textwidth}|p{.20\textwidth}|p{.23\textwidth}|}
  \hline
  {\bf Input} & {\bf Output } & {\bf Classes Covered}\\
  \hline
	{$ \alpha = 90$} & {right} &{ $1,4,6$ }\\
	{$\beta = FLT\_EPSILON$}&&\\
	{$\gamma = 90-FLT\_EPSILON$}&&\\\hline
	{$ \alpha = 180$} & {degenerate} &{ $1,3,6$ }\\
	{$\beta = 0$}&&\\
	{$\gamma = 0$}&&\\\hline
	{$ \alpha = -FLT\_EPSILON$} & {ERROR} &{ $2,6$ }\\
	{$\beta = FLT\_EPSILON$}&&\\
	{$\gamma = 180$}&&\\\hline
	{$ \alpha = 90$} & {ERROR} &{ $5,6,1$ }\\
	{$\beta = 0$}&&\\
	{$\gamma = 90$}&&\\\hline
	{$ \alpha = 45$} & {ERROR} &{ $1,4,7$ }\\
	{$\beta = 90-FLT\_EPSILON$}&&\\
	{$\gamma = 45$}&&\\\hline
	{$ \alpha = 180-FLT\_EPSILON$} & {ERROR} &{ $1,5,6$ }\\
	{$\beta = 0$}&&\\
	{$\gamma = FLT\_EPSILON$}&&\\\hline
	{$ \alpha = 90+FLT\_EPSILON$} & {ERROR} &{ $1,4,7$ }\\
	{$\beta = 45$}&&\\
	{$\gamma = 45$}&&\\\hline
	%{$ \alpha = 180 \\ \beta = 0 \\ \gamma = 0 $} & {degenerate} & {$1,3,6$} \\\hline
\end{tabular}

\newpage

\section{Invariants (4 points)}

\noindent
  Consider the following program, where {\tt r} and {\tt i} are integers
  and {\tt n} is a non-negative natural number (possibly 0):
  \begin{lstlisting}[language=C,basicstyle=\ttfamily\normalsize]
    int r = -1;
    int i = 0;
    while ((i <= n) && (r == -1)) {
      if (i * i == n)
        r = i;
      else
        i = i + 1;
    }
  \end{lstlisting}

Consider the formulas below; tick the correct box
($\Box\,\!\!\!\!\!\!\checkmark$) to indicate
whether they are loop invariants for the program above.
\begin{compactitem}
  \item If the formula is an inductive invariant, provide an
    (informal) argument that the invariant is inductive.
  \item If the formula $P$ is an invariant that is \emph{not} inductive,
    give values of {\tt r}, {\tt i} and {\tt n} before and after the loop
    body demonstrating that assertion 2 can fail even if we start
    in a state in which assertion 1 holds:
    \begin{displaymath}
      \underbrace{{\tt assert} (P \wedge B){\tt;}}_{\text{assertion 1}}\quad
      \underbrace{{\tt if\;(i * i == n)\;r = i;\;else\;i = i +
          1;}}_{\text{loop body}}\quad
      \underbrace{{\tt assert} (P){\tt;}}_{\text{assertion 2}}
    \end{displaymath}
    where $B=(i \leq n \land r = -1)$.
\item Otherwise,
  provide values of {\tt r}, {\tt i} and {\tt n} that correspond to
  a reachable state showing that the formula is \emph{not} an
  invariant.
\end{compactitem}
$\,$\\
\noindent
\begin{tabular}
  {|p{.2\textwidth}p{.25\textwidth}p{.3\textwidth}p{.15\textwidth}|}
\hline
$(r^2=n)\vee(r<0)$ & $\Box\,\!\!\!\!\!\!\checkmark$ Inductive Invariant & $\Box$ Non-inductive Invariant &
$\Box$ Neither\\[1ex]
\multicolumn{4}{|l|}{\textbf{Justification:}}\\
\multicolumn{4}{|l|}{
If $r$ is defined, then as $\sqrt{n}$. This means $r^2=n$. If that is not the case $r=-1<0$.
}\\
\hline
$(i^2\leq n)$ & $\Box$ Inductive Invariant & $\Box$ Non-inductive Invariant &
$\Box\,\!\!\!\!\!\!\checkmark$ Neither\\[1ex]
\multicolumn{4}{|l|}{\textbf{Justification:}}\\
\multicolumn{4}{|l|}{
If $n=2$ and $i=1$ then the assertion is fullfilled on the beginning of the loop.}\\
\multicolumn{4}{|l|}{After adding $1$ to $i$ $2^2>2$ which is against the assertion.}\\
\hline
$(r\leq 1)\vee(r\neq n)$ & $\Box\,\!\!\!\!\!\!\checkmark$ Inductive Invariant & $\Box$ Non-inductive Invariant &
$\Box$ Neither\\[1ex]
\multicolumn{4}{|l|}{\textbf{Justification:}} \\
\multicolumn{4}{|l|}{$\forall n>1 \in \mathbb{N}: n^2 \neq n$. ($r$ is always defined as $\sqrt{n}$)}\\
\multicolumn{4}{|l|}{This also counts for all $i < 0: i \in \mathbb{Z}$ because $r$ would be defined as $-\sqrt{n}$.}\\
\multicolumn{4}{|l|}{If $n=0$ then $r=0$ when $i$ gets $0$. Then $r \leq 1$ which makes the assertion true.}\\
\multicolumn{4}{|l|}{If $n=1$ then $r=1$ when $i$ gets $1$. Then $r \leq 1$ which makes the assertion true.}\\
\hline
$(r\geq -1)$ & $\Box$ Inductive Invariant & $\Box\,\!\!\!\!\!\!\checkmark$ Non-inductive Invariant &
$\Box$ Neither\\[1ex]
\multicolumn{4}{|l|}{\textbf{Justification:}}\\
\multicolumn{4}{|l|}{$i=-2$ and $n=4$}\\
\multicolumn{4}{|l|}{This makes after the Iteration $r=-2$ which is $\leq -1$ but not reachable in the code.}
\\
\hline
\end{tabular}


\vfill
\begin{center}
  \small{Please hand in your assignment via TUWEL (as a single PDF
    file) by April 5, 2017, 4pm.}
\end{center}

\end{document}
}
